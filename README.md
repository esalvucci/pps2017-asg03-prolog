# A Star Wars Adventure Game in Prolog

The code in star_wars_adventure_in_prolog.pl has been written as assignment for the PPS2017 exam.
It implements a simple Text Adventure Game which has Star Wars as theme and it draws inspiration from [Mayalinux](https://github.com/esalvucci/Mayalinux).

## How to play
An example of match is given by the predicate `play`.

To start a match use the goal `?- start.` and follow the printed instruction in the output tab.