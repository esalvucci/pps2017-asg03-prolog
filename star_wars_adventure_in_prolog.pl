% Universal fact to define an equipment.
equipment(E).

% The available equipments that could be taken by the player.
equipment_available(X):- search(tree(tree(nil, c3po, tree(nil, r2d2, nil)),spada_laser, tree(tree(nil, blaster, nil), sguscio, tree(nil, x_wing, nil))), X).

% The location in which the player is.
here(planet(P)).

% Fully relational predicate, both to get all the available planets and to search a specified planet.
planet(P):- search_in_list([death_star, dagobah, tatooine, coruscant, alderaan, endor, kessel, naboo], P).

% Fully relational predicate, both to get all the available siths and to search a specified sith.
sith(S):- search(tree(tree(nil, darth_vader, nil), darth_sidious, tree(nil, dooku, tree(nil, darth_maul, nil))), S).

% Predicate to take a specified object.	
take(Object):-
	equipment_available(Object),
	!,
	retractall(equipment(_)),     
	asserta(equipment(Object)),
	print_action_result([Object, ' preso/a']);
	print_action_result([Object, ' non disponibile']). 

% Succeed if the equipment object unifies whith the lightsaber.
have_lightsaber:- equipment(E), E = spada_laser.

% Succeed if the specified sith is darth vader.
face_up_to(sith(S)):- S = darth_vader.

% Change the current planet in which the player is.
go_to(planet(P)):-
	planet(P),
	!, % a sort of xor
	retractall(here(_)),
	asserta(here(planet(P))),
	print_action_result(['Sei su ', P])
	;
	print_action_result(['Il pianeta ', P, ' non fa parte del nostro universo']).

% Fully relational search in a list.
search_in_list([H|T], H).
search_in_list([_|T], X):- search_in_list(T, X).

% Fully relational search in a tree.
search(tree(_,E,_), E).
search(tree(L,_,_),E):- search(L,E).
search(tree(_,_,R),E):- search(R,E).

start :-
	level(0).

level(0) :-
	print_setting(0).

level(1):-
	have_lightsaber,
	!,
	go_to(planet(dagobah)),
	print_setting(1)
	;
	print('Scegli un altro equipaggiamento'), nl.

level(2):-
	here(planet(P)), P = death_star,
	!,
	print_setting(2)
	;
	print('Darth Vader non si trova su questo pianeta'), nl.

play:-
	start,
	% Level 0
	findall(Object, equipment_available(Object), L),
	take(spada_laser),
	% level 1
	level(1),
	go_to(planet(death_star)),
	% Level 2
	level(2),
	face_up_to(sith(darth_vader)),
	print('Darth Vader: Obi-Wan, ti ha insegnato bene.'), nl.

print_action_result([]):-
	print('.'),nl,nl.
print_action_result([H|T]):-
	print(H), print_action_result(T).	
		
print_setting(0):-
	print('                                                                              '), nl,
	print('                   Luke, inizia qui il tuo allenamento per                    '), nl,
	print('               diventare uno Jedi, come tuo padre prima di te!                '), nl,
	print('                                                                              '), nl,
	print('            Leila ha bisogno del tuo aiuto ed Obi-Wan Kenobi sta              '), nl,
	print('          diventando troppo vecchio per affrontare da solo l impero           '), nl,
	print('                                                                              '), nl,
	print('         L Alleanza ribelle ha in mano i piani della Morte Nera, la           '), nl,
	print('            terribile arma di distruzione di massa dell Impero.               '), nl,
	print('          __---__                                                             '), nl,
	print('       / .  . : .__ ........\                                         .       '), nl,
	print('      /........./  \  ........\  .  .                            .            '), nl,
	print('     / :  :   :| () |  :........\                 .        .                  '), nl,
	print('    :...........\__/... .:......|        .                                    '), nl,
	print('    |___________________|           .                     .                   '), nl,
	print('    |___________________|         .                                           '), nl,
	print('    :  :  :   :   :   :  :  : :                      .            .           '), nl,
	print('  .  \........................./   .            .                             '), nl,
	print('   .   \._........._........./  .     .                   .                   '), nl,
	print('          -..___....- ...../            .         .                           '), nl,
	print('                                                                              '), nl,
	print('    Prima di proseguire l allenamento ti serve un equipaggiamento adatto:     '), nl,
	print('           Alcune cose sono fondamentali per uno Jedi ma ricorda:             '), nl,
	print('     potrai scegliere solo un oggetto tra quelli mostrati in _solution_       '), nl,
	print('       usa il goal ?- findall(Object, equipment_available(Object), L).        '), nl,
	print('                  per vedere gli oggetti disponibili                          '), nl,
	print('          Una volta scelto l equipaggiamento prendilo con il goal             '), nl,
	print('                               ?- take(oggetto)	                             '), nl, nl, nl.

print_setting(1):-
	print('       L addestramento sara completato quando ti sarai confrontato        '), nl,
	print('                       con Darth Fener, tuo Padre                         '), nl,
	print('                    ____                                                  '), nl,
	print('                 _.  :  `._                                               '), nl,
	print('             .-. `.  ;   . `.-.                                           '), nl,
	print('    __      / : ___\ ;  /___ ; \      __                                  '), nl,
	print('  ,_  --.:__; .-. ;: : .-. :__;.--  _`,                                   '), nl,
	print('  : `.t  --..  <@.`;_   ,@>` ..--  j.  `;                                 '), nl,
	print('       `:-.._J  -.- L__ `--   L_..-;                                      '), nl,
	print('          -.__ ;  .-    -.  : __.-                                        '), nl,
	print('             L   /.------.\   J                                           '), nl,
	print('               -.    --    .-                                             '), nl,
	print('             __.l -:_JL_;- ;.__                                           '), nl,
	print('          .-j/ .;  ;      / . \ -.                                        '), nl,
	print('        .  /:`.  -.:     .-  . ;  `.                                      '), nl,
	print('             Il vigore di uno Jedi proviene dalla Forza.                  '), nl,
	print('          Collera, paura, aggressivita, il lato oscuro esse sono          '), nl,
	print('           Luke, non sottovalutare il potere dell Imperatore              '), nl,
	print('                                                                          '), nl,
	print('                 Usa il goal ?- go_to(planet(pianeta)).                   '), nl, 
	print('               per andare dove pensi si trovi Darth Vader                 '), nl, nl.
	
print_setting(2):-
	print('                                                                               '), nl,
	print('                                                                               '), nl,
	print('        L imperatore aveva ragione: non c era bisogno che l Impero ti          '), nl,
	print('  cercasse, saresti stato tu a raggiungere Darth Sidious e Darth Fener stessi  '), nl,
	print('                                                                               '), nl,
	print('               -#-       ---*#######***---++* - --+--**###**--+--------        '), nl,
	print('   -          -*#       --+*#######*******--*-  -*-*########+-----------+      '), nl,
	print('               #*        - *####*----+-+---------**--********--+---------------'), nl,
	print(' -       -    -#-        -*#*- --- --- -    ----+ -          -++---------------'), nl,
	print('++-+++-++++-- -#-       +*-   -            -*- +--               --------**-*--'), nl,
	print('------------+-*# -+-   -+   -     --        ##--*                 -*---*******-'), nl,
	print('-------------+##-- - -+          -##-      -*#---    -*+           -*---****-*-'), nl,
	print('--------------#--#**--         -+-*--      +*- -+--- --#            -*--*******'), nl,
	print('---------------*###-+        --**##**------*-- -++--++--*-           -***-*****'), nl,
	print('--------------*###++        --++--+--+---****---------*-+             *********'), nl,
	print('--------------###--          -+--       -###*-  ----+*##--+ -+        -********'), nl,
	print('-------------*##*+            --+-      -****+   -   +-*-+-            -*******'), nl,
	print('------------*###--             --+    +-****           -++- -           *******'), nl,
	print('*-----------###-+               -++  -***#-             -  -            -******'), nl,
	print('*---*------####-                 -++*****-                ---            -*****'), nl,
	print('**-****---*###-          -+++--  --+-*** -                 ++-            *****'), nl,
	print('***-***--*###*              -++++++-----       -    -      +**+      - -- -****'), nl,
	print('**-*****-##*-             +-----+--                    -            -+--- -****'), nl,
	print('**-*******-               -+++-                     -+--              -+--*****'), nl,
	print('**-*****-*-+-          ----+++--                ---   --            ----*******'), nl,
	print('******---*--+  -      -++++----+--             -   +-+*#-     - +----**********'), nl,
	print('**-***------    --   -+----------+---              -+-*##-     -+-----***------'), nl,
	print('#* -U *****---  -     ----*****------+++++----    +-***---         ++--*--++-++'), nl,
	print('#*******--++--- -        ---*###**-----------+-----*##*-              +--------'), nl,
	print('                                                                               '), nl,
	print('   Devi affrontare tuo padre, vedi del buono in lui e solo tu puoi salvarlo.   '), nl,
	print('            puoi farlo tornare sulla retta via, la via della Forza             '), nl,
	print('                                                                               '), nl,
	print('            usa il goal ?- face_up_to(sith(nome)). per iniziare                '), nl,
	print('                     il confronto con tuo padre                                '), nl, nl.
